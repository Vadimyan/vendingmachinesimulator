﻿using System;
using System.Windows.Forms;

namespace VendingMachineSimulator
{
    /// <summary>
    /// Кнопка для оплаты в торговом автомате
    /// </summary>
    public class PaymentButton : Button
    {
        /// <summary>
        /// Номинал вносимой монеты
        /// </summary>
        public int CoinNominal { get; private set; }

        /// <summary>
        /// Количество вносимых монет
        /// </summary>
        public int CoinCount { get; private set; }

        /// <summary>
        /// Зависимый от кнопки контрол. Аналогичен свойству Tag, но имеет
        /// явный тип определенную роль.
        /// </summary>
        public Control DependentControl { get; private set; }

        public PaymentButton()
        {
        }

        public PaymentButton(int nominal, int count)
        {
            CoinNominal = nominal;
            CoinCount = count;
        }

        /// <summary>
        /// Задаёт зависимый от кнопки контрол, чтобы иметь возможность взаимодействовать с ним
        /// из обработчиков событий этой кнопки
        /// </summary>
        /// <param name="control">зависимый контрол</param>
        public void SetDependentControl(Control control)
        {
            DependentControl = control;
        }
    }
}
