﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using VendingMachineSimulator.Properties;

namespace VendingMachineSimulator
{
    public partial class FormMain : Form
    {
        /// <summary>
        /// Кошелёк покупателя
        /// </summary>
        private readonly Purse _userPurse = new Purse();

        /// <summary>
        /// Торговый автомат с едой и напитками
        /// </summary>
        private readonly VendingMachine _machine = new VendingMachine(100);

        public FormMain()
        {
            InitializeComponent();

            _userPurse.AddCoin(1, 10);
            _userPurse.AddCoin(2, 30);
            _userPurse.AddCoin(5, 20);
            _userPurse.AddCoin(10, 15);

            _machine.Purse.AddCoin(1, 100);
            _machine.Purse.AddCoin(2, 100);
            _machine.Purse.AddCoin(5, 100);
            _machine.Purse.AddCoin(10, 100);

            _machine.PaymentChanged += VM_PaymentChanged;
            _machine.NotEnoughFunds += VM_NotEnoughFunds;
            _machine.ProductBought += VM_ProductBought;
            _machine.ProductAdded += VM_ProductAdded;
            _machine.NoShortMoney += VM_NoShortMoney;

            _machine.AddProduct(new DarkRoastCoffee(), 20);
            _machine.AddProduct(new OrangeJuice(), 15);
            _machine.AddProduct(new BlackTea(), 10);
            _machine.AddProduct(new Milk(new DarkRoastCoffee()), 20); //кофе с молоком
            _machine.AddProduct(new Whip(), 15);

            ShowVendingMachinePurse();
            ShowUserPurse();
        }



        /// <summary>
        /// Отображает содержимое кошелька автомата на форме (в целях тестирования)
        /// </summary>
        private void ShowVendingMachinePurse()
        {
            textBoxMachinePurse.Text = string.Empty;
            foreach (int coin in _machine.Purse.Coins)
            {
                textBoxMachinePurse.Text += string.Format(Resources.CoinInfoString, coin, _machine.Purse.CoinCount(coin));
                textBoxMachinePurse.Text += Environment.NewLine;
            }

            textBoxMachinePurse.SelectionStart = textBoxMachinePurse.TextLength;
            textBoxMachinePurse.SelectionLength = 0;
        }
        
        /// <summary>
        /// Отображает содержимое кошелька пользователя
        /// </summary>
        private void ShowUserPurse()
        {
            flowPanelUserPurse.Controls.Clear();
            int width = (flowPanelUserPurse.Width - flowPanelUserPurse.Margin.Left - flowPanelUserPurse.Margin.Right) / 2;
            Padding zeroPadding = new Padding(0);
            foreach (int coin in _userPurse.Coins)
            {
                int coinCount = _userPurse.CoinCount(coin);
                Label label = new Label()
                {
                    Width = width,
                    Text = string.Format(Resources.CoinInfoString, coin, coinCount),
                    TextAlign = ContentAlignment.MiddleCenter,
                    Margin = zeroPadding,
                    Padding = zeroPadding,
                    Font = Font //Отказываемся от наследования свойства у родительского контрола
                };

                PaymentButton pButton = new PaymentButton(coin, coinCount)
                {
                    Width = width,
                    Text = Resources.PaymentButtonText,
                    TextAlign = ContentAlignment.MiddleCenter,
                    Margin = zeroPadding,
                    Padding = zeroPadding,
                    Font = Font //Отказываемся от наследования свойства у родительского контрола
                };
                pButton.SetDependentControl(label);
                pButton.Click += PaymentButton_Click;

                flowPanelUserPurse.Controls.Add(label);
                flowPanelUserPurse.Controls.Add(pButton);
            }
        }

        /// <summary>
        /// Показывает ассортимент товаров, их стоимость и количество
        /// </summary>
        private void ShowAssortment()
        {
            flowPanelVM.Controls.Clear();

            foreach (IProduct p in _machine.Products)
                flowPanelVM.Controls.Add(CreatePLabel(p));
        }

        /// <summary>
        /// Создаёт контрол-метку с информацией о товаре и возможностью взаимодействия с ним
        /// </summary>
        /// <param name="product">продукт, который будет добавлен в автомат</param>
        /// <returns>интерактивная метка с товаром</returns>
        private ProductLabel CreatePLabel(IProduct product)
        {
            ProductLabel label = new ProductLabel(product)
            {
                Text = string.Format("В наличии {0} - {1} у.е. ({2} ед.)",
                                     product.GetProductName(),
                                     product.GetProductPrice(),
                                     _machine.ProductCount(product)),
                TextAlign = ContentAlignment.MiddleCenter,
                ColorMouseDown = Color.Green,
                ColorMouseHover = Color.LightGreen,
                ColorMouseLeave = BackColor,
                ColorMouseUp = Color.White,
                Width = flowPanelVM.Width  - 10
            };
            label.Click += ProductLabel_Click;

            return label;
        }



        /// <summary>
        /// Обрабатывает событие изменения внесённой суммы
        /// </summary>
        /// <param name="sender">торговый автомат</param>
        /// <param name="e">аргументы события по умолчанию</param>
        private void VM_PaymentChanged(object sender, EventArgs e)
        {
            labelSumma.Text = _machine.PaymentSum + " y.e.";
            ShowVendingMachinePurse();
        }

        /// <summary>
        /// Обрабатывает событие, когда недостаточно средств для покупки
        /// </summary>
        /// <param name="sender">торговый автомат</param>
        /// <param name="e">аргументы события по умолчанию</param>
        private void VM_NotEnoughFunds(object sender, EventArgs e)
        {
            MessageBox.Show(this, "Недостаточно средств для покупки", "Недостаточно средств",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Обрабатывает событие, когда происходит покупка товаров
        /// </summary>
        /// <param name="sender">торговый автомат</param>
        /// <param name="e">аргументы события по умолчанию</param>
        private void VM_ProductBought(object sender, EventArgs e)
        {
            MessageBox.Show(this, "Спасибо за покупку!", "Покупка совершена",
                MessageBoxButtons.OK, MessageBoxIcon.Information);

            ShowAssortment();
        }

        /// <summary>
        /// Обрабатывает событие в случае если у автомата нет денег для сдачи
        /// </summary>
        /// <param name="sender">торговый автомат</param>
        /// <param name="e">аргументы события по умолчанию</param>
        private void VM_NoShortMoney(object sender, EventArgs e)
        {
            MessageBox.Show(this, "Извините, у автомата нет сдачи. Обратитесь в службу поддержки!", "Проблема в обслуживании",
                MessageBoxButtons.OK, MessageBoxIcon.Information);

            ShowAssortment();
        }

        /// <summary>
        /// Обрабатывает событие при добавлении нового продукта
        /// </summary>
        /// <param name="sender">торговый автомат</param>
        /// <param name="e">аргументы события по умолчанию</param>
        private void VM_ProductAdded(object sender, VendingMachineEventArgs e)
        {
            flowPanelVM.Controls.Add(CreatePLabel(e.Product));
        }

        /// <summary>
        /// Обработчик клика по метке с товаром
        /// </summary>
        /// <param name="sender">интерактивная метка с товаром</param>
        /// <param name="e">аргументы события по умолчанию</param>
        private void ProductLabel_Click(object sender, EventArgs e)
        {
            ProductLabel label = sender as ProductLabel;
            _machine.SelectProduct(label.Product);

            label.Text = "Выбран товар: " + label.Product.GetProductName() + " (" + label.Product.GetProductPrice() + ")";
            label.Enabled = false;
        }

        /// <summary>
        /// Обрабатывает событие, когда покупатель вносит монету в торговый автомат
        /// </summary>
        /// <param name="sender">кнопка внесения оплаты</param>
        /// <param name="e">аргументы события по умолчанию</param>
        private void PaymentButton_Click(object sender, EventArgs e)
        {
            PaymentButton button = sender as PaymentButton;
            Label label = button.DependentControl as Label;

            int took = _userPurse.TakeMoney(button.CoinNominal, 1); //взять из кошелька покупателя
            int c = _userPurse.CoinCount(button.CoinNominal);
            if (c == 0) //если монет этого номинала больше нет, то убираем кнопку
            {
                button.Click -= PaymentButton_Click;
                button.SetDependentControl(null);
                flowPanelUserPurse.Controls.Remove(button);
                flowPanelUserPurse.Controls.Remove(label);
            }
            else
            {
                label.Text = string.Format(Resources.CoinInfoString, button.CoinNominal, c);
            }

            //внести монету
            _machine.AddPayment(button.CoinNominal, 1);
        }

        /// <summary>
        /// Обрабатывает событие, когда покупатель нажимает на кнопку получения сдачи
        /// </summary>
        /// <param name="sender">кнопка получения сдачи</param>
        /// <param name="e">аргументы события по умолчанию</param>
        private void ButtonShortMoney_Click(object sender, EventArgs e)
        {
            Purse tmp = _machine.GetShortMoney();
            if (tmp != null)
            {
                _userPurse.AddFromPurse(tmp);
                ShowUserPurse();
                ShowAssortment();
            }
        }

        /// <summary>
        /// Обрабатывает событие покупки товара
        /// </summary>
        /// <param name="sender">кнопка покупки товара</param>
        /// <param name="e">аргументы события по умолчанию</param>
        private void ButtonBuy_Click(object sender, EventArgs e)
        {
            _machine.BuyProducts();
        }
    }
}
