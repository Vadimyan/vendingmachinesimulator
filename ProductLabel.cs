﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;

namespace VendingMachineSimulator
{
    /// <summary>
    /// Интерактивная метка с информацией о продукте для автомата
    /// </summary>
    public class ProductLabel : Label
    {
        public IProduct Product { get; private set; }

        [Browsable(true)]
        public Color ColorMouseHover { get; set; }
        [Browsable(true)]
        public Color ColorMouseLeave { get; set; }
        [Browsable(true)]
        public Color ColorMouseDown { get; set; }
        [Browsable(true)]
        public Color ColorMouseUp { get; set; }
        [Browsable(true)]
        public Color BackColorDefault { get; set; }

        public ProductLabel() { }

        public ProductLabel(IProduct product)
        {
            Product = product;
        }

        protected override void OnMouseHover(EventArgs e)
        {
            base.OnMouseHover(e);

            BackColor = ColorMouseHover;
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);

            BackColor = ColorMouseLeave;
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            BackColor = ColorMouseDown;
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            BackColor = ColorMouseUp;
        }
    }
}
