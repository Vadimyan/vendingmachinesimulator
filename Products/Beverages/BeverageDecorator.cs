﻿using System;

namespace VendingMachineSimulator
{
    /// <summary>
    /// Дополнения и добавки к напиткам
    /// </summary>
    public abstract class BeverageDecorator : Beverage
    {
        protected const string WITH = " with ";
        protected Beverage _beverage;
    }
}
