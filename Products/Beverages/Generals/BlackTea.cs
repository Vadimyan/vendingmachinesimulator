﻿using System;

namespace VendingMachineSimulator
{
    /// <summary>
    /// Чёрный чай
    /// </summary>
    public class BlackTea : Beverage
    {
        private const string NAME = "Black Tea";

        public override string GetProductName()
        {
            return NAME;
        }

        public override int GetProductPrice()
        {
            return 13;
        }
    }
}
