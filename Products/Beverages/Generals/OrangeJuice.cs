﻿using System;

namespace VendingMachineSimulator
{
    /// <summary>
    /// Апельсиновый сок
    /// </summary>
    public class OrangeJuice : Beverage
    {
        private const string NAME = "Orange Juice";

        public override string GetProductName()
        {
            return NAME;
        }

        public override int GetProductPrice()
        {
            return 35;
        }
    }
}
