﻿using System;

namespace VendingMachineSimulator
{
    /// <summary>
    /// Кофе тёмной обжарки
    /// </summary>
    public class DarkRoastCoffee : Beverage
    {
        private const string NAME = "Dark roast coffee";

        public override string GetProductName()
        {
            return NAME;
        }

        public override int GetProductPrice()
        {
            return 18;
        }
    }
}
