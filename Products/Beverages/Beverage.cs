﻿using System;

namespace VendingMachineSimulator
{
    /// <summary>
    /// Представляет напиток, который можно продавать в автомате
    /// </summary>
    public abstract class Beverage : IProduct
    {
        public abstract string GetProductName();
        public abstract int GetProductPrice();
    }
}
