﻿using System;

namespace VendingMachineSimulator
{
    /// <summary>
    /// Молоко
    /// </summary>
    public class Milk : BeverageDecorator
    {
        private const string NAME = "Milk";
        private const int PRICE = 3;

        public Milk() { }

        public Milk(Beverage beverage)
        {
            Decorate(beverage);
        }

        public void Decorate(Beverage beverage)
        {
            _beverage = beverage;
        }

        public override string GetProductName()
        {
            if (_beverage == null)
                return NAME;

            return _beverage.GetProductName() + WITH + NAME;
        }

        public override int GetProductPrice()
        {
            if (_beverage == null)
                return PRICE;

            return _beverage.GetProductPrice() + PRICE;
        }
    }
}
