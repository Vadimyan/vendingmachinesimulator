﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VendingMachineSimulator
{
    /// <summary>
    /// Сливки
    /// </summary>
    class Whip : BeverageDecorator
    {
        private const string NAME = "Whip";
        private const int PRICE = 4;

        public Whip() { }

        public Whip(Beverage beverage)
        {
            Decorate(beverage);
        }

        public void Decorate(Beverage beverage)
        {
            _beverage = beverage;
        }

        public override string GetProductName()
        {
            if (_beverage == null)
                return NAME;

            return _beverage.GetProductName() + WITH + NAME;
        }

        public override int GetProductPrice()
        {
            if (_beverage == null)
                return PRICE;

            return _beverage.GetProductPrice() + PRICE;
        }
    }
}
