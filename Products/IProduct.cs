﻿using System;

namespace VendingMachineSimulator
{
    /// <summary>
    /// Интерфейс товара для торгового автомата
    /// </summary>
    public interface IProduct
    {
        /// <summary>
        /// Получает название товара
        /// </summary>
        /// <returns>название товара</returns>
        string GetProductName();

        /// <summary>
        /// Получает стоимость товара
        /// </summary>
        /// <returns>стоимость товара</returns>
        int GetProductPrice();
    }
}
