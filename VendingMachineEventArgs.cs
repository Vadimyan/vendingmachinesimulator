﻿using System;

namespace VendingMachineSimulator
{
    /// <summary>
    /// Агрументы события с информацией о продукте для торгового автомата
    /// </summary>
    public class VendingMachineEventArgs : EventArgs
    {
        /// <summary>
        /// Продукт (товар) из торгового автомата
        /// </summary>
        public IProduct Product { get; private set; }

        /// <summary>
        /// Создаёт аргументы события с заданной информацией
        /// </summary>
        /// <param name="p">товар - аргумент события</param>
        public VendingMachineEventArgs(IProduct p)
        {
            Product = p;
        }
    }
}
