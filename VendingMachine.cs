﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VendingMachineSimulator
{
    /// <summary>
    /// Торговый автомат для продажи напитков и снеков
    /// </summary>
    public class VendingMachine
    {
        /// <summary>
        /// Содержит список продуктов и их количество
        /// </summary>
        private readonly Dictionary<IProduct, int> _products = new Dictionary<IProduct, int>();

        /// <summary>
        /// Содержит список выбранных товаров
        /// </summary>
        private readonly List<IProduct> _selectedProducts = new List<IProduct>();

        /// <summary>
        /// Долг автомата, если ему не хватило денег для сдачи.
        /// </summary>
        private int _debt = 0;


        /// <summary>
        /// Возникает, когда изменяется сумма внесённых денег
        /// </summary>
        public event EventHandler<EventArgs> PaymentChanged;

        /// <summary>
        /// Возникает, когда покупатель выбирает товар
        /// </summary>
        public event EventHandler<EventArgs> ProductSelected;

        /// <summary>
        /// Возникает при добавлении товара в автомат
        /// </summary>
        public event EventHandler<VendingMachineEventArgs> ProductAdded;

        /// <summary>
        /// Возникает при удалении наименования или покупке всех доступных товаров одного вида
        /// </summary>
        public event EventHandler<VendingMachineEventArgs> ProductRemoved;

        /// <summary>
        /// Возникает когда недостаточно средств для покупки
        /// </summary>
        public event EventHandler<EventArgs> NotEnoughFunds;

        /// <summary>
        /// Возникает при покупке товара
        /// </summary>
        public event EventHandler<EventArgs> ProductBought;

        /// <summary>
        /// Возникает, когда у автомата недостаточно денег для сдачи
        /// </summary>
        public event EventHandler<EventArgs> NoShortMoney;



        /// <summary>
        /// Получает сумму внесённых средств
        /// </summary>
        public int PaymentSum { get; private set; }

        /// <summary>
        /// Получает сумму денег для сдачи
        /// </summary>
        public int ShortMoneySum { get; private set; }

        /// <summary>
        /// Получает список продуктов в автомате (без учёта количества каждого вида)
        /// </summary>
        public IEnumerable<IProduct> Products { get { return _products.Keys; } }

        /// <summary>
        /// Кошелёк внутри автомата
        /// </summary>
        internal Purse Purse { get; private set; }

        /// <summary>
        /// Ограничение по количеству продуктов
        /// </summary>
        public int ProductLimit { get; private set; }

        /// <summary>
        /// Количество продуктов в автомате
        /// </summary>
        public int Count { get; private set; }



        /// <summary>
        /// Создаёт объект торгового автомата с заданной вместимостью продуктов
        /// </summary>
        /// <param name="limit">максимальное количество продуктов</param>
        public VendingMachine(int limit)
        {
            Purse = new Purse();
            ProductLimit = limit;
        }


        /// <summary>
        /// Позволяет внести деньги в автомат
        /// </summary>
        /// <param name="ye">номинал вносимой монеты (купюры)</param>
        /// <param name="count">количество монет</param>
        public void AddPayment(int ye, int count)
        {
            PaymentSum += ye * count;
            ShortMoneySum += ye * count;
            Purse.AddCoin(ye, count); //во всех автоматах монеты сразу попадают в их внутреннюю копилку.
            OnPaymentChanged();
        }

        /// <summary>
        /// Добавляет продукты в автомат
        /// </summary>
        /// <param name="products">коллекция продуктов</param>
        public void AddProducts(IEnumerable<IProduct> products)
        {
            foreach (IProduct product in products)
                AddProduct(product, 1);
        }

        /// <summary>
        /// Добавляет продукт в автомат
        /// </summary>
        /// <param name="product">продукт (товар)</param>
        /// <param name="count">количество единиц</param>
        public void AddProduct(IProduct product, int count)
        {
            if (Count + count >= ProductLimit)
                return;

            int value = 0;
            if (_products.TryGetValue(product, out value))
            {
                _products[product] += count;
            }
            else
            {
                _products.Add(product, count);
            }

            Count += count;
            OnProductAdded(product);
        }

        /// <summary>
        /// Получает количество продуктов заданного типа
        /// </summary>
        /// <param name="product">продукт (товар)</param>
        /// <returns>количество единиц</returns>
        public int ProductCount(IProduct product)
        {
            int value = 0;
            _products.TryGetValue(product, out value);
            return value;
        }

        /// <summary>
        /// Позволяет выбрать продукт
        /// </summary>
        /// <param name="product">выбираемый продукт (товар)</param>
        public void SelectProduct(IProduct product)
        {
            int value = 0;
            if (_products.TryGetValue(product, out value))
            {
                _selectedProducts.Add(product);
                ShortMoneySum -= product.GetProductPrice();
            }
            else
                throw new ArgumentException("Попытка выбрать продукт, которого нет в автомате");

            OnProductSelected();
        }

        /// <summary>
        /// Осуществляет покупку выбранных ранее продуктов
        /// </summary>
        public void BuyProducts()
        {
            if (_selectedProducts.Count == 0)
                return;

            //Если недостаточно средств для покупки
            if (_selectedProducts.Sum(p => p.GetProductPrice()) > PaymentSum)
            {
                OnNotEnoughFunds();
                return;
            }

            //Удаляем (покупаем) выбранные продукты из автомата
            foreach (IProduct product in _selectedProducts)
            {
                int count = --_products[product];
                if (count == 0)
                {
                    _products.Remove(product);
                    OnProductRemoved(product);
                }
            }

            PaymentSum = ShortMoneySum;
            _selectedProducts.Clear();

            OnPaymentChanged();
            OnProductBought();
        }

        /// <summary>
        /// Позволяет получить сдачу
        /// </summary>
        /// <returns>кошелёк со сдачей или null если получать нечего</returns>
        public Purse GetShortMoney()
        {
            if (PaymentSum <= 0)
                return null;

            //Если пользователь передумал покупать
            if (_selectedProducts.Count != 0)
                ShortMoneySum = PaymentSum;

            Purse purse = new Purse();
            IEnumerable<int> coins = Purse.Coins.OrderByDescending(coin => coin); //упорядочиваем монеты по уменьшению номинала
            foreach (int coin in coins)
            {
                int coinCount = ShortMoneySum / coin; //требуемое кол-во монет номинала coin
                if (coinCount == 0)
                    continue;

                int tookMoney = Purse.TakeMoney(coin, coinCount); //количество взятых монет из кошелька автомата
                purse.AddCoin(coin, tookMoney);
                ShortMoneySum -= (coin * tookMoney);

                if (ShortMoneySum == 0)
                    break;
            }

            //Если денег для сдачи не хватило
            if (ShortMoneySum != 0)
            {
                _debt = ShortMoneySum;
                OnNoShortMoney();
            }

            PaymentSum = ShortMoneySum = 0;

            OnPaymentChanged();
            return purse;
        }



        /// <summary>
        /// Вызывает событие ProductRemoved
        /// </summary>
        /// <param name="p">аргумент события - удалённое наименование товара</param>
        protected virtual void OnProductRemoved(IProduct p)
        {
            var tmp = ProductRemoved;
            if (tmp != null)
                tmp(this, new VendingMachineEventArgs(p));
        }

        /// <summary>
        /// Вызывает событие NoShortMoney
        /// </summary>
        protected virtual void OnNoShortMoney()
        {
            var tmp = NoShortMoney;
            if (tmp != null)
                tmp(this, EventArgs.Empty);
        }

        /// <summary>
        /// Вызывает событие ProductBought
        /// </summary>
        protected virtual void OnProductBought()
        {
            var tmp = ProductBought;
            if (tmp != null)
                tmp(this, EventArgs.Empty);
        }

        /// <summary>
        /// Вызывает событие NotEnoughFunds
        /// </summary>
        protected virtual void OnNotEnoughFunds()
        {
            var tmp = NotEnoughFunds;
            if (tmp != null)
                tmp(this, EventArgs.Empty);
        }

        /// <summary>
        /// Вызывает событие PaymentChanged
        /// </summary>
        protected virtual void OnPaymentChanged()
        {
            var tmp = PaymentChanged;
            if (tmp != null)
                tmp(this, EventArgs.Empty);
        }

        /// <summary>
        /// Вызывает событие ProductSelected
        /// </summary>
        protected virtual void OnProductSelected()
        {
            var tmp = ProductSelected;
            if (tmp != null)
                tmp(this, EventArgs.Empty);
        }

        /// <summary>
        /// Вызывает событие ProductAdded
        /// </summary>
        protected virtual void OnProductAdded(IProduct p)
        {
            var tmp = ProductAdded;
            if (tmp != null)
                tmp(this, new VendingMachineEventArgs(p));
        }
    }
}
