﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace VendingMachineSimulator
{
    /// <summary>
    /// Кошелёк с деньгами. Сюда можно добавить также возможность
    /// брать деньги только тому, для кого предназначен этот кошелёк, чтобы защититься от кражи
    /// </summary>
    public class Purse
    {
        /// <summary>
        /// Список монет с их номиналом и количествами
        /// </summary>
        private readonly Dictionary<int, int> _coins = new Dictionary<int, int>();

        /// <summary>
        /// Получает список номиналов монет доступный для перечисления
        /// </summary>
        public IEnumerable<int> Coins { get { return _coins.Keys; } }

        /// <summary>
        /// Создаёт пустой кошелек
        /// </summary>
        public Purse() { }

        /// <summary>
        /// Получает количество монет заданного номинала
        /// </summary>
        /// <param name="nominal">номинал монеты</param>
        /// <returns>количество монет</returns>
        public int CoinCount(int nominal)
        {
            int value = 0;
            if (_coins.TryGetValue(nominal, out value))
                return _coins[nominal];

            return 0;
        }

        /// <summary>
        /// Добавляет все деньги из другого кошелька
        /// </summary>
        /// <param name="purse"></param>
        public void AddFromPurse(Purse purse)
        {
            foreach (int coin in purse.Coins)
                AddCoin(coin, purse.CoinCount(coin));
        }

        /// <summary>
        /// Добавляет монеты в кошелёк
        /// </summary>
        /// <param name="nominal">номинал монеты</param>
        /// <param name="count">количество штук</param>
        public void AddCoin(int nominal, int count)
        {
            if (count <= 0)
                return;

            int value = 0;
            if (_coins.TryGetValue(nominal, out value))
            {
                _coins[nominal] += count;
            }
            else
            {
                _coins.Add(nominal, count);
            }
        }

        /// <summary>
        /// Забирает монеты из кошелька
        /// </summary>
        /// <param name="nominal">номинал монеты</param>
        /// <param name="count">количество монет</param>
        /// <returns>количество монет, которые удалось взять. Это значение
        /// может быть меньше указанного, если указанное число больше имеющегося
        /// количества монет в кошельке</returns>
        public int TakeMoney(int nominal, int count)
        {
            int value = 0;
            if (_coins.TryGetValue(nominal, out value))
            {
                if (value <= count)
                {
                    _coins.Remove(nominal);
                    return value;
                }

                _coins[nominal] -= count;
                return count;
            }

            //Если монет заданного номинала нет, то и брать нечего
            return 0;
        }
    }
}
