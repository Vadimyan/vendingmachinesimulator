﻿namespace VendingMachineSimulator
{
    partial class FormMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxMachinePurse = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.flowPanelUserPurse = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.flowPanelVM = new System.Windows.Forms.FlowLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.labelSumma = new System.Windows.Forms.Label();
            this.buttonShortMoney = new System.Windows.Forms.Button();
            this.buttonBuy = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxMachinePurse
            // 
            this.textBoxMachinePurse.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBoxMachinePurse.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxMachinePurse.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBoxMachinePurse.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxMachinePurse.Location = new System.Drawing.Point(7, 58);
            this.textBoxMachinePurse.Multiline = true;
            this.textBoxMachinePurse.Name = "textBoxMachinePurse";
            this.textBoxMachinePurse.ReadOnly = true;
            this.textBoxMachinePurse.Size = new System.Drawing.Size(313, 142);
            this.textBoxMachinePurse.TabIndex = 0;
            this.textBoxMachinePurse.Text = "Test";
            this.textBoxMachinePurse.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.flowPanelUserPurse);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxMachinePurse);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(546, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(326, 387);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Кошельки:";
            // 
            // flowPanelUserPurse
            // 
            this.flowPanelUserPurse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowPanelUserPurse.Location = new System.Drawing.Point(7, 223);
            this.flowPanelUserPurse.Name = "flowPanelUserPurse";
            this.flowPanelUserPurse.Size = new System.Drawing.Size(311, 158);
            this.flowPanelUserPurse.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(6, 203);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(167, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Кошелёк пользователя:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(6, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Деньги в автомате:";
            // 
            // flowPanelVM
            // 
            this.flowPanelVM.AutoScroll = true;
            this.flowPanelVM.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowPanelVM.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.flowPanelVM.ForeColor = System.Drawing.Color.Navy;
            this.flowPanelVM.Location = new System.Drawing.Point(12, 29);
            this.flowPanelVM.Margin = new System.Windows.Forms.Padding(0);
            this.flowPanelVM.Name = "flowPanelVM";
            this.flowPanelVM.Size = new System.Drawing.Size(528, 338);
            this.flowPanelVM.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Внесённая сумма:";
            // 
            // labelSumma
            // 
            this.labelSumma.AutoSize = true;
            this.labelSumma.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSumma.Location = new System.Drawing.Point(146, 9);
            this.labelSumma.Name = "labelSumma";
            this.labelSumma.Size = new System.Drawing.Size(43, 17);
            this.labelSumma.TabIndex = 5;
            this.labelSumma.Text = "0 y.e.";
            // 
            // buttonShortMoney
            // 
            this.buttonShortMoney.Location = new System.Drawing.Point(184, 373);
            this.buttonShortMoney.Name = "buttonShortMoney";
            this.buttonShortMoney.Size = new System.Drawing.Size(175, 23);
            this.buttonShortMoney.TabIndex = 6;
            this.buttonShortMoney.Text = "Забрать сдачу";
            this.buttonShortMoney.UseVisualStyleBackColor = true;
            this.buttonShortMoney.Click += new System.EventHandler(this.ButtonShortMoney_Click);
            // 
            // buttonBuy
            // 
            this.buttonBuy.Location = new System.Drawing.Point(365, 373);
            this.buttonBuy.Name = "buttonBuy";
            this.buttonBuy.Size = new System.Drawing.Size(175, 23);
            this.buttonBuy.TabIndex = 7;
            this.buttonBuy.Text = "Купить товары";
            this.buttonBuy.UseVisualStyleBackColor = true;
            this.buttonBuy.Click += new System.EventHandler(this.ButtonBuy_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 405);
            this.Controls.Add(this.buttonBuy);
            this.Controls.Add(this.buttonShortMoney);
            this.Controls.Add(this.labelSumma);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.flowPanelVM);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.ShowIcon = false;
            this.Text = "Модель автомата для продажи напитков и снеков";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxMachinePurse;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowPanelUserPurse;
        private System.Windows.Forms.FlowLayoutPanel flowPanelVM;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelSumma;
        private System.Windows.Forms.Button buttonShortMoney;
        private System.Windows.Forms.Button buttonBuy;

    }
}

